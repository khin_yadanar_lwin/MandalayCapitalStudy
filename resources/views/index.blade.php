<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mandalay Capital</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/font-awesome.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
</head>
<body>
    <div class="container">
        <div class="nav-bar">
            <div class="logo-container">
                <img src="/images/Logo3.png" alt="">
            </div>
            <div id="myLinks" class="nav-items">
                <a href="">Home</a>
                <a href="">what we offer?</a>
                <a href="">09891497256</a>
                <a href=""><img src="/images/myanmar-flag-icon-free-download.jpg" alt="mmflag"></a>
                <a href=""><img src="/images/uk.png" alt="ukflag"></a>
            </div>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                <i class="fa fa-bars"></i>
            </a>
        </div>
    </div>
    <div class="banner">
        <div class="banner-item item-1">
            <div class="Let-us-help-your-Bus">
                Let us help your Business
                <br>
                with Financing
            </div>
            <div class="support-text-for-moto">
            Starting an SME from scratch is hard work. 
            <br>
            Achieve new heights for your business with our business loan.
            </div>
            <div class="rectangle">
                Expand Today
            </div>
        </div>
        <div class="banner-item item-2">
                <img class="img-1" src="/images/banner-img-1.jpeg" alt="">
                <img class="img-2" src="/images/banner-img-2.jpg" alt="">
                <img class="img-3" src="/images/banner-img-3.jpg" alt="">
        </div>
    </div>
<script>
        document.addEventListener("DOMContentLoaded", function() {
        document.querySelector(".banner .item-2 .img-1").classList.add('active');
        document.querySelector(".banner .item-2 .img-2").classList.add('active');
        document.querySelector(".banner .item-2 .img-3").classList.add('active');
        });
    function myFunction()
    {
        $('.nav-items').toggle();
  }
</script>
</body>
</html>